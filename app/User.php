<?php

namespace App;

use App\Task;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    /**
     * @var array
     */
    protected $hidden = [

        'password', 'remember_token',
    ];

    public function tasks()

    {
        return $this->hasMany(Task::class);
    }

}
