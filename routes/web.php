<?php


Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/tasks', 'TaskController@index');
Route::post('/task', 'TaskController@store');
Route::delete('/task/{task}', 'TaskController@destroy');

Route::auth();


Auth::routes();

